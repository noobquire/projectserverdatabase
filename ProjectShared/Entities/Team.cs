﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectShared.Entities
{
    public class Team
    {
        public int Id { get; set; }
        [Required]
        [MinLength(10)]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
    }
}
