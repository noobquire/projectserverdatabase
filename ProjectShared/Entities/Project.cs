﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectShared.Entities
{
    public class Project
    {
        public int Id { get; set; }
        [Required]
        [MinLength(10)]
        [MaxLength(150)]
        public string Name { get; set; }
        [MinLength(50)]
        [MaxLength(1000)]
        public string Description { get; set; }
        public DateTime StartedAt { get; set; }
        public DateTime Deadline { get; set; }
        [Required]
        public User Author { get; set; }
        public Team Team { get; set; }
        public IList<UserProject> Users { get; set; }
    }
}
