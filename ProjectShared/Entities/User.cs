﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectShared.Entities
{
    public class User
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(20)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(30)]
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        [Required]
        public DateTime RegisteredAt { get; set; }
        public Team Team { get; set; }
        public IList<UserProject> Projects { get; set; }
    }
}
