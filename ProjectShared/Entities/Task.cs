﻿using ProjectShared.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectShared.Entities
{
    public class Task
    {
        public int Id { get; set; }
        [Required]
        [MinLength(10)]
        [MaxLength(150)]
        public string Name { get; set; }
        [MinLength(30)]
        [MaxLength(500)]
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime CompletedAt { get; set; }
        [Required]
        public TaskState State { get; set; }
        public Project Project { get; set; }
        public User Performer { get; set; }
    }
}