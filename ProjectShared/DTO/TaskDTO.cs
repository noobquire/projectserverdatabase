﻿using System;
using Newtonsoft.Json;

namespace ProjectShared.DTO
{
    [Serializable]
    public class TaskDTO
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty(PropertyName = "finished_at")]
        public DateTime FinishedAt { get; set; }

        [JsonProperty(PropertyName = "state")]
        public TaskState State { get; set; }

        [JsonProperty(PropertyName = "project_id")]
        public int ProjectId { get; set; }

        [JsonProperty(PropertyName = "performer_id")]
        public int PerformerId { get; set; }
    }
}