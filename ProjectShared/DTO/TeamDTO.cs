﻿using System;
using Newtonsoft.Json;

namespace ProjectShared.DTO
{
    [Serializable]
    public class TeamDTO
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }
    }
}