﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectShared.DTO;
using ProjectShared.Entities;

namespace ProjectManager
{
    public static class Api
    {
        private static readonly HttpClient client = new HttpClient();
        private static readonly string apiAddress = "https://localhost:5001/api";

        public static async Task<IEnumerable<ProjectDTO>> GetProjectsAsync()
        {
            var response = await client.GetAsync($"{apiAddress}/projects");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IEnumerable<ProjectDTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<ProjectDTO> GetProjectAsync(int id)
        {
            var response = await client.GetAsync($"{apiAddress}/projects/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<ProjectDTO>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IEnumerable<TaskDTO>> GetTasksAsync()
        {
            var response = await client.GetAsync($"{apiAddress}/tasks");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<TaskDTO> GetTaskAsync(int id)
        {
            var response = await client.GetAsync($"{apiAddress}/tasks/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<TaskDTO>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IEnumerable<TeamDTO>> GetTeamsAsync()
        {
            var response = await client.GetAsync($"{apiAddress}/teams");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IEnumerable<TeamDTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<TeamDTO> GetTeamAsync(int id)
        {
            var response = await client.GetAsync($"{apiAddress}/teams/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<TeamDTO>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IEnumerable<UserDTO>> GetUsersAsync()
        {
            var response = await client.GetAsync($"{apiAddress}/users");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IEnumerable<UserDTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<UserDTO> GetUserAsync(int id)
        {
            var response = await client.GetAsync($"{apiAddress}/users/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<UserDTO>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IEnumerable<MessageRecord>> GetAllRecords()
        {
            var response = await client.GetAsync($"{apiAddress}/messages");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IEnumerable<MessageRecord>>(await response.Content.ReadAsStringAsync());
        }
    }
}