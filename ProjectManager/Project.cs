﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectShared.DTO;

namespace ProjectManager
{
    public class Project
    {
        public readonly List<Task> Tasks;

        public Project(ProjectDTO dto)
        {
            Tasks = Api.GetTasksAsync().Result.Where(t => t.ProjectId == dto.Id)
                .Select(t => new Task(t, false) {Project = this}).ToList();
            Name = dto.Name;
            var author = Api.GetUserAsync(dto.AuthorId).Result;
            Author = new User(author);
            Team = new Team(Api.GetTeamAsync(dto.TeamId).Result);
            Name = dto.Name;
            Description = dto.Description;
            CreatedAt = dto.CreatedAt;
            Deadline = dto.Deadline;
        }

        public User Author { get; set; }
        public Team Team { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
    }
}