﻿using System;
using ProjectShared.DTO;

namespace ProjectManager
{
    public class Task
    {
        /// <summary>
        ///     Creates a new task based on deserialised JSON object
        /// </summary>
        /// <param name="dto">JSON object</param>
        /// <param name="setProject">
        ///     Defines whether if to load Project property.
        ///     Used to prevent Project and Task creating instances of each other.
        /// </param>
        public Task(TaskDTO dto, bool setProject = true)
        {
            Name = dto.Name;
            Description = dto.Description;
            CreatedAt = dto.CreatedAt;
            FinishedAt = dto.FinishedAt;
            State = dto.State;
            if (setProject) Project = new Project(Api.GetProjectAsync(dto.ProjectId).Result);
            Performer = new User(Api.GetUserAsync(dto.PerformerId).Result);
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
        public Project Project { get; set; }
        public User Performer { get; set; }
    }
}