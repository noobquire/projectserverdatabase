﻿using System;
using ProjectShared.DTO;

namespace ProjectManager
{
    public class Team
    {
        public Team(TeamDTO dto)
        {
            Name = dto.Name;
            CreatedAt = dto.CreatedAt;
        }

        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}