﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using ProjectWorker.Interfaces;
using ProjectShared.Entities;

namespace ProjectWorker.Services
{
    class FileService : IFileService
    {
        private readonly IConfiguration _configuration;
        private string _filePath;
        private IQueueService _queue;

        public FileService(IConfiguration configuration, IQueueService queue)
        {
            _configuration = configuration;
            Configure();
            _queue = queue;
        }
        private void Configure()
        {
            _filePath = _configuration.GetSection("LogFile").Value;
        }
        public void WriteRecord(string message)
        {
            var record = new MessageRecord
            {
                Message = message,
                DateRecieved = DateTime.Now,
            };


            StatusRecord writeResult = new StatusRecord
            {
                Success = true,
                Message = "Successfully recieved"
            };
            try
            {
                var recordsList = File.Exists(_filePath) ? 
                    JsonConvert.DeserializeObject<IList<MessageRecord>>(File.ReadAllText(_filePath)) 
                    : new List<MessageRecord>();
                recordsList.Add(record);
                File.WriteAllText(_filePath, JsonConvert.SerializeObject(recordsList));
            }
            catch(ArgumentNullException)
            {
                writeResult.Success = false;
                writeResult.Message = "Error: file path was null";
            }
            catch (UnauthorizedAccessException)
            {
                writeResult.Success = false;
                writeResult.Message = "Error: no permissions to read/write the log file";
            }
            catch (IOException)
            {
                writeResult.Success = false;
                writeResult.Message = "Error reading/writing the log file";
            }
            catch(JsonException)
            {
                writeResult.Success = false;
                writeResult.Message = "Error parsing the JSON string";
            }
            finally
            {
                var statusMessage = JsonConvert.SerializeObject(writeResult);
                Console.WriteLine($"Sending status message: {statusMessage}");
                _queue.PostValue(statusMessage);
            }
        }
    }
}
