﻿using Microsoft.Extensions.Configuration;
using ProjectWorker.Interfaces;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectWorker.Services
{
    public sealed class MessageService : IMessageService
    {
        private readonly IConfiguration _configuration;

        private EventingBasicConsumer _consumer;
        private IModel _channel;
        private IConnection _connection;
        private IFileService _fileService;

        public MessageService(IConfiguration configuration, IFileService fileService)
        {
            _configuration = configuration;
            _fileService = fileService;
        }

        public void Run()
        {
            Console.WriteLine("Starting message service");
            Configure();
        }

        private void Configure()
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(_configuration.GetSection("RabbitMQ").Value),
            };


            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare("RequestExchange", ExchangeType.Direct);


            _channel.QueueDeclare(
                queue: "RequestQueue",
                durable: true,
                exclusive: false,
                autoDelete: false
                );
            _channel.QueueBind("RequestQueue", "RequestExchange", "request");

            _consumer = new EventingBasicConsumer(_channel);
            _consumer.Received += MessageRecieved;

            _channel.BasicConsume("RequestQueue", autoAck: false, consumer: _consumer);

        }

        private void MessageRecieved(object sender, BasicDeliverEventArgs e)
        {
            byte[] body = e.Body;
            string message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"Recieved {message}");
            _fileService.WriteRecord(message);

            _channel.BasicAck(e.DeliveryTag, false);
        }

        public void Dispose()
        {
            _connection?.Dispose();
            _channel?.Dispose();
        }
    }
}
