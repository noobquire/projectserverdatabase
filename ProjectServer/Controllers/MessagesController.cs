﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectServer.Interfaces;
using ProjectShared.Entities;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private IFileService _fileService;
        public MessagesController(IFileService fileService)
        {
            _fileService = fileService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<MessageRecord>> GetAllRecords()
        {
            return Ok(_fileService.GetMessageRecords());
        }
    }
}