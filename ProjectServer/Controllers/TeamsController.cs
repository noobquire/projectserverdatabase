﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectShared.DTO;
using ProjectServer.DAL;
using ProjectServer.Services;
using ProjectServer.Interfaces;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITeamsService _teamsService;
        private readonly IQueueService _queueService;

        public TeamsController(IUnitOfWork unitOfWork, ITeamsService teamsService, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _teamsService = teamsService;
            _queueService = queueService;
        }

        // GET: api/Tasks
        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> Get()
        {
            return Ok(_teamsService.GetAll());
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            return Ok(_teamsService.Get(id));
        }

        // POST: api/Tasks
        [HttpPost]
        public ActionResult Post([FromBody] TeamDTO value)
        {
            _teamsService.Create(value);
            return Ok();
        }

        [HttpPost]
        [Route("CreateMany")]
        public ActionResult Post([FromBody] IEnumerable<TeamDTO> values)
        {
            _teamsService.CreateMany(values);
            return Ok();
        }

        // PUT: api/Tasks
        [HttpPut]
        public ActionResult Put([FromBody] TeamDTO value)
        {
            _teamsService.Update(value);
            return Ok();
        }

        // DELETE: api/Tasks/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _teamsService.Delete(id);
            return Ok();
        }

        [HttpGet]
        [Route("WithUsersOlderThan12")]
        public ActionResult<IEnumerable<UsersInTeamDTO>> GetTeamsWithUsersOlderThan12()
        {
            return Ok(_teamsService.GetTeamsWithUsersOlderThan12());
        }
    }
}