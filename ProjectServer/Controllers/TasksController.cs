﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectShared.DTO;
using ProjectServer.DAL;
using ProjectServer.Interfaces;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITasksService _tasksService;

        public TasksController(ITasksService tasksService)
        {
            _tasksService = tasksService;
        }

        // GET: api/Tasks
        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> Get()
        {
            return Ok(_tasksService.GetAll());
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public ActionResult<TaskDTO> Get(int id)
        {
            return Ok(_tasksService.Get(id));
        }

        // POST: api/Tasks
        [HttpPost]
        public ActionResult Post([FromBody] TaskDTO value)
        {
            _tasksService.Create(value);
            return Ok();
        }

        [HttpPost]
        [Route("CreateMany")]
        public ActionResult Post([FromBody] IEnumerable<TaskDTO> values)
        {
            _tasksService.CreateMany(values);
            return Ok();
        }

        // PUT: api/Tasks
        [HttpPut]
        public ActionResult Put([FromBody] TaskDTO value)
        {
            _tasksService.Update(value);
            return Ok();
        }

        // DELETE: api/Tasks/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _tasksService.Delete(id);
            return Ok();
        }
    }
}