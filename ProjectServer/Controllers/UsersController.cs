﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectShared.DTO;
using ProjectServer.DAL;
using ProjectServer.Interfaces;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;

        public UsersController(IUnitOfWork unitOfWork, IUsersService usersService)
        {
            _usersService = usersService;
        }

        // GET: api/Users
        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> Get()
        {
            return Ok(_usersService.GetAll());
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public ActionResult<UserDTO> Get(int id)
        {
            return Ok(_usersService.Get(id));
        }

        // POST: api/Users
        [HttpPost]
        public ActionResult Post([FromBody] UserDTO value)
        {
            _usersService.Create(value);
            return Ok();
        }

        [HttpPost]
        [Route("CreateMany")]
        public ActionResult Post([FromBody] IEnumerable<UserDTO> values)
        {
            _usersService.CreateMany(values);
            return Ok();
        }

        // PUT: api/Users
        [HttpPut]
        public ActionResult Put([FromBody] UserDTO value)
        {
            _usersService.Update(value);
            return Ok();
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _usersService.Delete(id);
            return Ok();
        }

        // GET: api/Users/5/TaskCount
        [HttpGet]
        [Route("{id}/TaskCount")]
        public ActionResult<Dictionary<ProjectDTO, int>> GetTaskCount(int id)
        {
            return Ok(_usersService.GetTaskCount(id));
        }

        // GET: api/Users/5/TasksWithNameLengthLessThan45
        [HttpGet]
        [Route("{id}/TasksWithNameLengthLessThan45")]
        public ActionResult<IEnumerable<TaskDTO>> GetTasksWithNameLengthLessThan45(int id)
        {
            return Ok(_usersService.GetTasksWithNameLengthLessThan45(id));
        }

        // GET: api/Users/5/TasksFinishedIn2019
        [HttpGet]
        [Route("{id}/TasksFinishedIn2019")]
        public ActionResult<IEnumerable<TaskDTO>> GetTasksFinishedIn2019(int id)
        {
            
            return Ok(_usersService.GetTasksFinishedIn2019(id));
        }

        // GET: api/Users/5/Stats
        [HttpGet]
        [Route("{id}/Stats")]
        public ActionResult<UserStatsDTO> GetUserStats(int id)
        {
            
            return Ok(_usersService.GetUserStats(id));
        }

        // GET: api/Users/UsersSortedByFirstNameWithTasksSortedByNameLength
        [HttpGet]
        [Route("SortedByFirstNameWithTasksSortedByNameLength")]
        public ActionResult<IEnumerable<TasksOfUserDTO>> GetUsersSortedByFirstNameWithTasksSortedByNameLength()
        {
            
            return Ok(_usersService.GetUsersSortedByFirstNameWithTasksSortedByNameLength());
        }
    }
}