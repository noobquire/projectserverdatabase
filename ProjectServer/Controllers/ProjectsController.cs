﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectServer.DAL;
using ProjectServer.Interfaces;
using System;
using ProjectShared.DTO;

namespace ProjectServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsService _projectsService;
        private readonly IMessageService _messageService;
        public ProjectsController(IProjectsService projectsService, IMessageService messageService)
        {
            _projectsService = projectsService;
            _messageService = messageService;
        }

        // GET: api/Projects
        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> Get()
        {
            return Ok(_projectsService.GetAll());
        }

        // GET: api/Projects/5
        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            return Ok(_projectsService.Get(id));
        }

        // POST: api/Projects
        [HttpPost]
        public ActionResult Post([FromBody] ProjectDTO value)
        {
            _projectsService.Create(value);
            return Ok();
        }

        [HttpPost]
        [Route("CreateMany")]
        public ActionResult Post([FromBody] IEnumerable<ProjectDTO> values)
        {
            _projectsService.CreateMany(values);
            return Ok();
        }

        // PUT: api/Projects/5
        [HttpPut]
        public ActionResult Put([FromBody] ProjectDTO value)
        {
            _projectsService.Update(value);
            return Ok();
        }

        // DELETE: api/Projects/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _projectsService.Delete(id);
            return Ok();
        }

        // GET: api/Projects/5/Stats
        [HttpGet]
        [Route("{id}/Stats")]
        public ActionResult<ProjectStatsDTO> GetStats(int id)
        {
            return Ok(_projectsService.GetProjectStats(id));
        }
    }
}