﻿namespace ProjectServer.Interfaces
{
    public interface IQueueService
    {
        bool PostValue(string message);

    }
}
