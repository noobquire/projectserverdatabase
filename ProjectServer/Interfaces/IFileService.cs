﻿using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectServer.Interfaces
{
    public interface IFileService
    {
        IEnumerable<MessageRecord> GetMessageRecords();
    }
}
