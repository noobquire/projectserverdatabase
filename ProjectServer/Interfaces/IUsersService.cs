﻿using ProjectShared.DTO;
using System.Collections.Generic;

namespace ProjectServer.Interfaces
{
    public interface IUsersService : ICrudService<UserDTO>
    {
        /// <summary>
        /// Get count of tasks per each project which was created by this user
        /// </summary>
        /// <returns>Key: project ID, value: amount of tasks</returns>
        IDictionary<int, int> GetTaskCount(int userId);
        IEnumerable<TaskDTO> GetTasksWithNameLengthLessThan45(int userId);
        IEnumerable<TaskDTO> GetTasksFinishedIn2019(int userId);
        UserStatsDTO GetUserStats(int userId);
        IEnumerable<TasksOfUserDTO> GetUsersSortedByFirstNameWithTasksSortedByNameLength();
    }
}
