﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectServer.Interfaces
{
    public interface ICrudService<T>
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        void Create(T item);
        void CreateMany(IEnumerable<T> items);
        void Update(T item);
        void Delete(int id);
    }
}
