﻿using RabbitMQ.Client.Events;
using System;

namespace ProjectServer.Interfaces
{
    public interface IMessageService : IDisposable
    {
        void Configure();
        void MessageRecieved(object sender, BasicDeliverEventArgs e);
    }
}
