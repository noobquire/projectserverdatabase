using ProjectShared.DTO;
using System.Collections.Generic;

namespace ProjectServer.Interfaces
{
    public interface ITeamsService : ICrudService<TeamDTO>
    {
        IEnumerable<UsersInTeamDTO> GetTeamsWithUsersOlderThan12();
    }
}
