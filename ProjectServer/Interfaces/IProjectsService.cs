﻿using ProjectShared.DTO;
using System.Collections.Generic;

namespace ProjectServer.Interfaces
{
    public interface IProjectsService : ICrudService<ProjectDTO>
    {
        ProjectStatsDTO GetProjectStats(int projectId);
    }
}
