﻿using ProjectServer.Interfaces;
using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectServer.DAL
{
    public class DbProjectRepository : IRepository<Project>
    {
        private ProjectServerDbContext _context;
        public DbProjectRepository(ProjectServerDbContext context)
        {
            _context = context;
        }
        public void Create(Project item)
        {
            if (_context.Projects.Any(p => p.Id == item.Id)) throw new ArgumentException("Item already exists");
            _context.Projects.Add(item);
        }

        public void Create(IEnumerable<Project> items)
        {
            foreach (var item in items) Create(item);
        }

        public void Delete(int id)
        {
            _context.Projects.Remove(Get(id));
        }

        public Project Get(int id)
        {
            return _context.Projects.Single(p => p.Id == id);
        }

        public IEnumerable<Project> GetAll()
        {
            return _context.Projects;
        }

        public void Update(Project item)
        {
            var project = _context.Projects.Single(p => p.Id == item.Id);
            project = item;
        }
    }
}
