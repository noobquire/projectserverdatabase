﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectShared.DTO;
using ProjectShared.Entities;
using ProjectServer.Interfaces;

namespace ProjectServer.DAL
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly List<Project> _projects = new List<Project>();

        public void Create(Project item)
        {
            if (_projects.Any(p => p.Id == item.Id)) throw new ArgumentException("Item already exists");
            _projects.Add(item);
        }

        public void Create(IEnumerable<Project> items)
        {
            foreach (var item in items) Create(item);
        }

        public void Delete(int id)
        {
            _projects.RemoveAt(_projects.FindIndex(p => p.Id == id));
        }

        public Project Get(int id)
        {
            return _projects.Find(p => p.Id == id);
        }

        public IEnumerable<Project> GetAll()
        {
            return _projects;
        }

        public void Update(Project item)
        {
            _projects[_projects.FindIndex(p => p.Id == item.Id)] = item;
        }
    }
}