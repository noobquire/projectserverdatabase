﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectShared.DTO;
using ProjectServer.Interfaces;
using ProjectShared.Entities;

namespace ProjectServer.DAL
{
    public class TaskRepository : IRepository<Task>
    {
        private readonly List<Task> _tasks = new List<Task>();

        public void Create(Task item)
        {
            if (_tasks.Any(t => t.Id == item.Id)) throw new ArgumentException("Item already exists");
            _tasks.Add(item);
        }

        public void Create(IEnumerable<Task> items)
        {
            foreach (var item in items) Create(item);
        }

        public void Delete(int id)
        {
            _tasks.RemoveAt(_tasks.FindIndex(t => t.Id == id));
        }

        public Task Get(int id)
        {
            return _tasks.Find(t => t.Id == id);
        }

        public IEnumerable<Task> GetAll()
        {
            return _tasks;
        }

        public void Update(Task item)
        {
            _tasks[_tasks.FindIndex(t => t.Id == item.Id)] = item;
        }
    }
}