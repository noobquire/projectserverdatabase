﻿using ProjectServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using ProjectShared.Entities;

namespace ProjectServer.DAL
{
    public class DbTaskRepository : IRepository<Task>
    {
        private ProjectServerDbContext _context;
        public DbTaskRepository(ProjectServerDbContext context)
        {
            _context = context;
        }
        public void Create(Task item)
        {
            if (_context.Tasks.Any(p => p.Id == item.Id)) throw new ArgumentException("Item already exists");
            _context.Tasks.Add(item);
        }

        public void Create(IEnumerable<Task> items)
        {
            foreach (var item in items) Create(item);
        }

        public void Delete(int id)
        {
            _context.Tasks.Remove(Get(id));
        }

        public Task Get(int id)
        {
            return _context.Tasks.Single(p => p.Id == id);
        }

        public IEnumerable<Task> GetAll()
        {
            return _context.Tasks;
        }

        public void Update(Task item)
        {
            var task = _context.Tasks.Single(p => p.Id == item.Id);
            task = item;
        }
    }
}
