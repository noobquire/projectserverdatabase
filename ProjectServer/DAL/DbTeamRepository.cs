﻿using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectServer.DAL
{
    public class DbTeamRepository
    {
        private ProjectServerDbContext _context;
        public DbTeamRepository(ProjectServerDbContext context)
        {
            _context = context;
        }
        public void Create(Team item)
        {
            if (_context.Teams.Any(p => p.Id == item.Id)) throw new ArgumentException("Item already exists");
            _context.Teams.Add(item);
        }

        public void Create(IEnumerable<Team> items)
        {
            foreach (var item in items) Create(item);
        }
        public void Delete(int id)
        {
            _context.Teams.Remove(Get(id));
        }

        public Team Get(int id)
        {
            return _context.Teams.Single(p => p.Id == id);
        }

        public IEnumerable<Team> GetAll()
        {
            return _context.Teams;
        }

        public void Update(Team item)
        {
            var team = _context.Teams.Single(p => p.Id == item.Id);
            team = item;
        }
    }
}
