﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectShared.DTO;
using ProjectServer.Interfaces;
using ProjectShared.Entities;

namespace ProjectServer.DAL
{
    public class UserRepository : IRepository<User>
    {
        private readonly List<User> _users = new List<User>();

        public void Create(User item)
        {
            if (_users.Any(u => u.Id == item.Id)) throw new ArgumentException("Item already exists");
            _users.Add(item);
        }

        public void Create(IEnumerable<User> items)
        {
            foreach (var item in items) Create(item);
        }

        public void Delete(int id)
        {
            _users.RemoveAt(_users.FindIndex(u => u.Id == id));
        }

        public User Get(int id)
        {
            return _users.Find(u => u.Id == id);
        }

        public IEnumerable<User> GetAll()
        {
            return _users;
        }

        public void Update(User item)
        {
            _users[_users.FindIndex(u => u.Id == item.Id)] = item;
        }
    }
}