﻿using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectServer.DAL
{
    public class DbUserRepository
    {
        private ProjectServerDbContext _context;
        public DbUserRepository(ProjectServerDbContext context)
        {
            _context = context;
        }
        public void Create(User item)
        {
            if (_context.Users.Any(p => p.Id == item.Id)) throw new ArgumentException("Item already exists");
            _context.Users.Add(item);
        }

        public void Create(IEnumerable<User> items)
        {
            foreach (var item in items) Create(item);
        }
        public void Delete(int id)
        {
            _context.Users.Remove(Get(id));
        }

        public User Get(int id)
        {
            return _context.Users.Single(p => p.Id == id);
        }

        public IEnumerable<User> GetAll()
        {
            return _context.Users;
        }

        public void Update(User item)
        {
            var user = _context.Users.Single(p => p.Id == item.Id);
            user = item;
        }
    }
}
