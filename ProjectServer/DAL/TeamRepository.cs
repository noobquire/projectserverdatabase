﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectShared.DTO;
using ProjectServer.Interfaces;
using ProjectShared.Entities;

namespace ProjectServer.DAL
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly List<Team> _teams = new List<Team>();

        public void Create(Team item)
        {
            if (_teams.Any(t => t.Id == item.Id)) throw new ArgumentException("Item already exists");
            _teams.Add(item);
        }

        public void Create(IEnumerable<Team> items)
        {
            foreach (var item in items) Create(item);
        }

        public void Delete(int id)
        {
            _teams.RemoveAt(_teams.FindIndex(t => t.Id == id));
        }

        public Team Get(int id)
        {
            return _teams.Find(t => t.Id == id);
        }

        public IEnumerable<Team> GetAll()
        {
            return _teams;
        }

        public void Update(Team item)
        {
            _teams[_teams.FindIndex(t => t.Id == item.Id)] = item;
        }
    }
}