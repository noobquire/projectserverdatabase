﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace ProjectServer.Hubs
{
    public class RequestHub : Hub
    {
        public async Task SendMessage(string message)
        {
            await Clients.All.SendAsync("RecieveMessage", message);
        }
    }
}
