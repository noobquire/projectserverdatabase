﻿using Microsoft.Extensions.Configuration;
using ProjectServer.Interfaces;
using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProjectServer.Services
{
    public class FileService : IFileService
    {
        private IConfiguration _configuration;

        public FileService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IEnumerable<MessageRecord> GetMessageRecords()
        {
            string filePath = _configuration.GetSection("LogFile").Value;
            return File.Exists(filePath) ? JsonConvert.DeserializeObject<IEnumerable<MessageRecord>>(File.ReadAllText(filePath)) : new List<MessageRecord>();
        }
    }
}
