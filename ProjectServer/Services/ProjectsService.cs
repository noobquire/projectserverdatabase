﻿using System.Linq;
using ProjectShared.DTO;
using ProjectServer.Interfaces;
using System.Collections.Generic;
using AutoMapper;
using ProjectShared.Entities;
using System;

namespace ProjectServer.Services
{
    public class ProjectsService : IProjectsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IQueueService _queueService;

        public ProjectsService(IUnitOfWork unitOfWork, IMapper mapper, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _queueService = queueService;
        }

        public void Create(ProjectDTO item)
        {
            _queueService.PostValue("Creating a project was triggered");
            _unitOfWork.Projects.Create(_mapper.Map<Project>(item));
            _unitOfWork.SaveChanges();
        }

        public void CreateMany(IEnumerable<ProjectDTO> items)
        {
            _queueService.PostValue("Creating many projects was triggered");
            _unitOfWork.Projects.Create(_mapper.Map<Project[]>(items));
            _unitOfWork.SaveChanges();
        }

        public void Delete(int id)
        {
            _queueService.PostValue("Deleting a project was triggered");
            _unitOfWork.Projects.Delete(id);
            _unitOfWork.SaveChanges();
        }

        public ProjectDTO Get(int id)
        {
            _queueService.PostValue("Getting a project by id was triggered");
            return _mapper.Map<ProjectDTO>(_unitOfWork.Projects.Get(id));
        }

        public IEnumerable<ProjectDTO> GetAll()
        {
            _queueService.PostValue("Getting all projects was triggered");
            return _mapper.Map<ProjectDTO[]>(_unitOfWork.Projects.GetAll());
        }

        public ProjectStatsDTO GetProjectStats(int projectId)
        {
            _queueService.PostValue("Getting project stats was triggered");
            return (from project in _unitOfWork.Projects.GetAll()
                    join task in _unitOfWork.Tasks.GetAll() on project.Id equals task.Project.Id into projectTasks
                    join user in _unitOfWork.Users.GetAll() on project.Team.Id equals user.Team.Id into projectTeam
                    select new ProjectStatsDTO
                    {
                        ProjectId = project.Id,

                        TaskWithLongestDescId = projectTasks.Any() ? projectTasks
                        .OrderByDescending(t => t.Description.Length)
                        .FirstOrDefault().Id : -1,

                        TaskWithShortestNameId = projectTasks.Any() ? projectTasks
                        .OrderBy(t => t.Name.Length)
                        .FirstOrDefault().Id : -1,

                        TotalUsersInProjectTeam =
                        (project.Description.Length > 25 || projectTasks.Count() < 3) ?
                        projectTeam.Count() : 0,
                    }).FirstOrDefault(ps => ps.ProjectId == projectId);

        }

        public void Update(ProjectDTO item)
        {
            _queueService.PostValue("Updating a project was triggered");
            _unitOfWork.Projects.Update(_mapper.Map<Project>(item));
            _unitOfWork.SaveChanges();
        }
    }
}
