﻿using System.Collections.Generic;
using System.Linq;
using ProjectServer.DAL;
using ProjectShared.DTO;
using ProjectServer.Interfaces;
using AutoMapper;
using ProjectShared.Entities;

namespace ProjectServer.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IQueueService _queueService;

        public UsersService(IUnitOfWork unitOfWork, IMapper mapper, IQueueService queueService)
        {
            _queueService = queueService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(UserDTO item)
        {
            _queueService.PostValue("Creating a project was triggered");
            _unitOfWork.Users.Create(_mapper.Map<User>(item));
            _unitOfWork.SaveChanges();
        }

        public void CreateMany(IEnumerable<UserDTO> items)
        {
            _queueService.PostValue("Creating many projects was triggered");
            _unitOfWork.Users.Create(_mapper.Map<User[]>(items));
            _unitOfWork.SaveChanges();
        }

        public void Delete(int id)
        {
            _queueService.PostValue("Deleting a project was triggered");
            _unitOfWork.Users.Delete(id);
            _unitOfWork.SaveChanges();
        }

        public UserDTO Get(int id)
        {
            _queueService.PostValue("Getting a project by id was triggered");
            return _mapper.Map<UserDTO>(_unitOfWork.Users.Get(id));
        }

        public IEnumerable<UserDTO> GetAll()
        {
            _queueService.PostValue("Getting all projects was triggered");
            return _mapper.Map<UserDTO[]>(_unitOfWork.Users.GetAll());
        }

        public void Update(UserDTO item)
        {
            _queueService.PostValue("Updating a project was triggered");
            _unitOfWork.Users.Update(_mapper.Map<User>(item));
            _unitOfWork.SaveChanges();
        }

        public IDictionary<int, int> GetTaskCount(int userId)
        {
            _queueService.PostValue("Getting count of tasks per each project of user was triggered");
            return _unitOfWork.Projects.GetAll()
                .Where(p => p.Author.Id == userId)
                .ToDictionary(p => p.Id, p => _unitOfWork.Tasks.GetAll()
                .Where(t => t.Project.Id == p.Id).Count());
        }

        public IEnumerable<TaskDTO> GetTasksFinishedIn2019(int userId)
        {
            _queueService.PostValue("Getting tasks of a user finished in 2019 was triggered");
            return _mapper.Map<TaskDTO[]>(_unitOfWork.Tasks.GetAll().Where(t => t.Performer.Id == userId
                && t.State == TaskState.Finished
                && t.CompletedAt.Year == 2019));
        }

        public IEnumerable<TasksOfUserDTO> GetUsersSortedByFirstNameWithTasksSortedByNameLength()
        {
            _queueService.PostValue("Getting users sorted by first name with their tasks sorted by name length was triggered");
            return _unitOfWork.Users.GetAll()
                .OrderBy(u => u.FirstName)
                .Select(u => new TasksOfUserDTO
                {
                    UserId = u.Id,
                    TasksOfUserIds = _unitOfWork.Tasks.GetAll()
                    .Where(t => t.Performer.Id == u.Id)
                    .OrderByDescending(t => t.Name.Length)
                    .Select(t => t.Id),
                });
        }

        public UserStatsDTO GetUserStats(int userId)
        {
            _queueService.PostValue("Getting user stats was triggered");
            return _unitOfWork.Projects.GetAll()
                .Where(p => p.Author.Id == userId)
                .OrderByDescending(p => p.StartedAt)
                .Select(p => new UserStatsDTO
                {
                    UserId = userId,
                    LastProjectId = p.Id,
                    LastProjectTasksCount = _unitOfWork.Tasks.GetAll()
                    .Where(t => t.Project.Id == p.Id).Count(),
                    CancelledAndUnfinishedTasksCount = _unitOfWork.Tasks.GetAll()
                    .Where(t => t.Project.Id == p.Id && t.State != TaskState.Finished).Count(),
                    LongestTaskId = _unitOfWork.Tasks.GetAll()
                    .Where(t => t.Project.Id == p.Id)
                    .OrderBy(t => t.CreatedAt)
                    .ThenByDescending(t => t.CompletedAt)
                    .FirstOrDefault().Id,
                }).FirstOrDefault();
        }

        public IEnumerable<TaskDTO> GetTasksWithNameLengthLessThan45(int userId)
        {
            _queueService.PostValue("Getting of a user tasks with name length less than 45 symbols was triggered");
            return _mapper.Map<TaskDTO[]>(_unitOfWork.Tasks.GetAll().Where(t => t.Performer.Id == userId && t.Name.Length < 45));
        }

    }
}
