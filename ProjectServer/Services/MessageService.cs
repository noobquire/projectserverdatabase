﻿using Microsoft.Extensions.Configuration;
using ProjectServer.Hubs;
using ProjectServer.Interfaces;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.SignalR;

namespace ProjectServer.Services
{
    public sealed class MessageService : IMessageService
    {
        private readonly IConfiguration _configuration;

        private EventingBasicConsumer _consumer;
        private IModel _channel;
        private IConnection _connection;
        private readonly IHubContext<RequestHub> _hubContext;

        public MessageService(IConfiguration configuration, IHubContext<RequestHub> hubContext)
        {
            _configuration = configuration;
            _hubContext = hubContext;
            Configure();
        }

        public void Configure()
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(_configuration.GetSection("RabbitMQ").Value),
            };


            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare("StatusExchange", ExchangeType.Direct);


            _channel.QueueDeclare(
                queue: "StatusQueue",
                durable: true,
                exclusive: false,
                autoDelete: false);

            _channel.QueueBind("StatusQueue", "StatusExchange", "status");

            _consumer = new EventingBasicConsumer(_channel);
            _consumer.Received += MessageRecieved;

            _channel.BasicConsume("StatusQueue", autoAck: false, consumer: _consumer);

        }

        public void MessageRecieved(object sender, BasicDeliverEventArgs e)
        {
            byte[] body = e.Body;
            string message = Encoding.UTF8.GetString(body);

            Console.WriteLine($"Recieved {message}");

            // send to client via signalr
            _hubContext.Clients.All.SendAsync("RecieveMessage", message);

            _channel.BasicAck(e.DeliveryTag, false);
        }

        public void Dispose()
        {
            _connection?.Dispose();
            _channel?.Dispose();
        }
    }
}
