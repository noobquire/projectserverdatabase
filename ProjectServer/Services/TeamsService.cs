﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectServer.DAL;
using ProjectShared.DTO;
using ProjectServer.Interfaces;
using AutoMapper;
using ProjectShared.Entities;

namespace ProjectServer.Services
{
    public class TeamsService : ITeamsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IQueueService _queueService;

        public TeamsService(IUnitOfWork unitOfWork, IMapper mapper, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _queueService = queueService;
        }

        public void Create(TeamDTO item)
        {
            _queueService.PostValue("Creating a project was triggered");
            _unitOfWork.Teams.Create(_mapper.Map<Team>(item));
            _unitOfWork.SaveChanges();
        }

        public void CreateMany(IEnumerable<TeamDTO> items)
        {
            _queueService.PostValue("Creating many projects was triggered");
            _unitOfWork.Teams.Create(_mapper.Map<Team[]>(items));
            _unitOfWork.SaveChanges();
        }

        public void Delete(int id)
        {
            _queueService.PostValue("Deleting a project was triggered");
            _unitOfWork.Teams.Delete(id);
            _unitOfWork.SaveChanges();
        }

        public TeamDTO Get(int id)
        {
            _queueService.PostValue("Getting a project by id was triggered");
            return _mapper.Map<TeamDTO>(_unitOfWork.Teams.Get(id));
        }

        public IEnumerable<TeamDTO> GetAll()
        {
            _queueService.PostValue("Getting all projects was triggered");
            return _mapper.Map<TeamDTO[]>(_unitOfWork.Teams.GetAll());
        }

        public void Update(TeamDTO item)
        {
            _queueService.PostValue("Updating a project was triggered");
            _unitOfWork.Teams.Update(_mapper.Map<Team>(item));
            _unitOfWork.SaveChanges();
        }

        IEnumerable<UsersInTeamDTO> ITeamsService.GetTeamsWithUsersOlderThan12()
        {
            _queueService.PostValue("Getting teams with users older than 12 was triggered");
            return 
                
                _unitOfWork.Users.GetAll()
                .Where(u => DateTime.Now.Year - u.Birthday.Year > 12)
                .OrderByDescending(u => DateTime.Now.Ticks - u.RegisteredAt.Ticks)
                .GroupBy(u => u.Team.Id)
                .Select(g => new UsersInTeamDTO
                {
                    TeamId = g.Key,
                    TeamName = _unitOfWork.Teams.Get(g.Key).Name,
                    UsersInTeamIds = g.Select(u => u.Id),
                });
        }
    }
}
