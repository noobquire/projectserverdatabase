﻿using System.Linq;
using ProjectShared.DTO;
using ProjectServer.Interfaces;
using System.Collections.Generic;
using AutoMapper;
using ProjectShared.Entities;
using System;

namespace ProjectServer.Services
{
    public class TasksService : ITasksService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IQueueService _queueService;

        public TasksService(IUnitOfWork unitOfWork, IMapper mapper, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _queueService = queueService;
        }

        public void Create(TaskDTO item)
        {
            _queueService.PostValue("Creating a tassk was triggered");
            _unitOfWork.Tasks.Create(_mapper.Map<Task>(item));
            _unitOfWork.SaveChanges();
        }

        public void CreateMany(IEnumerable<TaskDTO> items)
        {
            _queueService.PostValue("Creating many tasks was triggered");
            _unitOfWork.Tasks.Create(_mapper.Map<Task[]>(items));
            _unitOfWork.SaveChanges();
        }

        public void Delete(int id)
        {
            _queueService.PostValue("Deleting a task was triggered");
            _unitOfWork.Tasks.Delete(id);
            _unitOfWork.SaveChanges();
        }

        public TaskDTO Get(int id)
        {
            _queueService.PostValue("Getting a task by id was triggered");
            return _mapper.Map<TaskDTO>(_unitOfWork.Tasks.Get(id));
        }

        public IEnumerable<TaskDTO> GetAll()
        {
            _queueService.PostValue("Getting all tasks was triggered");
            return _mapper.Map<TaskDTO[]>(_unitOfWork.Tasks.GetAll());
        }

        public void Update(TaskDTO item)
        {
            _queueService.PostValue("Updating a task was triggered");
            _unitOfWork.Tasks.Update(_mapper.Map<Task>(item));
            _unitOfWork.SaveChanges();
        }
    }
}
