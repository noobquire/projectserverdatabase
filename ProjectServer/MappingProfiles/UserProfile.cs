﻿using AutoMapper;
using ProjectShared.DTO;
using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectServer.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(dest => dest.TeamId, opts => opts.MapFrom(src => src.Team.Id));
            CreateMap<int, User>().ConvertUsing<EntityConverter<User>>();
            CreateMap<UserDTO, User>();
        }
    }
}
