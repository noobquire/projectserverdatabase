﻿using AutoMapper;
using ProjectShared.DTO;
using ProjectShared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectServer.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<int, Team>().ConvertUsing<EntityConverter<Team>>();
            CreateMap<TeamDTO, Team>();
        }
    }
}
